package Item;

import RandomGenerator.RandomGenerator;

public enum ItemName {
    MagicHat,
    WarriorGloves,
    KnightArmor,
    AngryCloak,
    NightShift,
    VoidHit,
    UniverseCore;

    public static ItemName getRandomItemName() {
        int index = RandomGenerator.getRandomNumber(0, values().length - 1);
        return values()[index];
    }
}
