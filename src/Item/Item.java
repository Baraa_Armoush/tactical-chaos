package Item;

import Arena.Square;
import Champion.Champion;

import java.io.Serializable;

public class Item {
    private ItemName name;
    private transient Champion currentChampion;
    private transient Square position;

    public Item() {
        this(null, null, null);
    }
    public Item(ItemName name, Champion currentChampion, Square position) {
        this.name = name;
        this.currentChampion = currentChampion;
        this.position = position;
    }

    public ItemName getName() {
        return name;
    }
    public Champion getCurrentChampion() {
        return currentChampion;
    }
    public Square getPosition() {
        return position;
    }

    public void setName(ItemName name) {
        this.name = name;
    }
    public void setCurrentChampion(Champion currentChampion) {
        this.currentChampion = currentChampion;
    }
    public void setPosition(Square position) {
        this.position = position;
    }
}
