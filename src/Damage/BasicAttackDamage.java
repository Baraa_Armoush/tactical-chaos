package Damage;

import Champion.Champion;

import java.io.Serializable;

public class BasicAttackDamage extends Damage {
    private double attackDamage;

    public BasicAttackDamage() {
        this(0.0);
    }
    public BasicAttackDamage(double attackDamage) {
        this.attackDamage = attackDamage;
    }

    public double getAttackDamage() {
        return attackDamage;
    }
    public void setAttackDamage(double attackDamage) {
        this.attackDamage = attackDamage;
    }

    @Override
    public void ApplyDamage(Champion champion) {
        double currentHealth = champion.getAttributes().getHealth();
        double currentArmor = champion.getArmor();
        champion.getAttributes().setHealth(Math.max(0.0, currentHealth - (1.0 - currentArmor) * attackDamage));
    }
}
