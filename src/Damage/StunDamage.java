package Damage;

import Champion.Champion;

import java.io.Serializable;

public class StunDamage extends Damage {
    private int stunnedRounds;

    public StunDamage() {
        this(0);
    }
    public StunDamage(int stunnedRounds) {
        this.stunnedRounds = stunnedRounds;
    }

    public int getStunnedRounds() {
        return stunnedRounds;
    }
    public void setStunnedRounds(int stunnedRounds) {
        this.stunnedRounds = stunnedRounds;
    }

    @Override
    public void ApplyDamage(Champion champion) {
        int currentStunnedDamage = champion.getStunnedRounds();
        champion.setStunnedRounds(Math.max(currentStunnedDamage, stunnedRounds));
    }
}
