package Damage;

import Champion.Champion;

import java.io.Serializable;

public abstract class Damage {
    public Damage() {

    }
    public abstract void ApplyDamage(Champion champion);
}
