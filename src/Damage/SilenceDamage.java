package Damage;

import Champion.Champion;

import java.io.Serializable;

public class SilenceDamage extends Damage {
    private int silenceRounds;

    public SilenceDamage() {
        this(0);
    }
    public SilenceDamage(int silenceRounds) {
        this.silenceRounds = silenceRounds;
    }

    public int getSilenceRounds() {
        return silenceRounds;
    }
    public void setSilenceRounds(int silenceRounds) {
        this.silenceRounds = silenceRounds;
    }

    @Override
    public void ApplyDamage(Champion champion) {
        int currentSilencedDamage = champion.getSilencedRounds();
        champion.setSilencedRounds(Math.max(currentSilencedDamage, silenceRounds));
    }
}
