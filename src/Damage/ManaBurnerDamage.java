package Damage;

import Champion.Champion;

import java.io.Serializable;

public class ManaBurnerDamage extends Damage {
    private double burnerDamage;

    public ManaBurnerDamage() {
        this(0.0);
    }
    public ManaBurnerDamage(double burnerDamage) {
        this.burnerDamage = burnerDamage;
    }

    public double getBurnerDamage() {
        return burnerDamage;
    }
    public void setBurnerDamage(double burnerDamage) {
        this.burnerDamage = burnerDamage;
    }

    @Override
    public void ApplyDamage(Champion champion) {
        double currentMana = champion.getAttributes().getCurrentMana();
        champion.getAttributes().setCurrentMana(Math.max(0.0, currentMana - burnerDamage));
    }
}
