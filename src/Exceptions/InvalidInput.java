package Exceptions;

public class InvalidInput extends Exception {
    public InvalidInput() {
        this("Exception - Invalid Input");
    }
    public InvalidInput(String message) {
        super(message);
    }
}
