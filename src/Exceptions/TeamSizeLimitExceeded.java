package Exceptions;

public class TeamSizeLimitExceeded extends Exception {
    public TeamSizeLimitExceeded() {
        this("Exception - Team size limit exceeded");
    }
    public TeamSizeLimitExceeded(String message) {
        super(message);
    }
}
