package Exceptions;

public class EnemyIsNotInAttackRange extends Exception {
    public EnemyIsNotInAttackRange() {
        this("Exception - Enemy is not in Attack Range");
    }
    public EnemyIsNotInAttackRange(String message) {
        super(message);
    }
}
