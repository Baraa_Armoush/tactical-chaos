package Exceptions;

public class NoEnoughMoney extends Exception {
    public NoEnoughMoney() {
        this("Exception - You have no enough money");
    }
    public NoEnoughMoney(String message) {
        super(message);
    }
}
