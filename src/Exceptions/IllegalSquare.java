package Exceptions;

public class IllegalSquare extends Exception {
    public IllegalSquare() {
        this("Exception - Illegal Square");
    }
    public IllegalSquare(String message) {
        super(message);
    }
}
