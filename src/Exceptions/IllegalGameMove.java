package Exceptions;

public class IllegalGameMove extends Exception {
    public IllegalGameMove() {
        this("Exception - Illegal Game Move");
    }
    public IllegalGameMove(String message) {
        super(message);
    }
}
