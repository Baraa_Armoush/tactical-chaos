package Exceptions;

import java.io.Serializable;

public class ChampionNotFound extends Exception {
    public ChampionNotFound() {
        this("Exception - Champion not found");
    }
    public ChampionNotFound(String message) {
        super(message);
    }
}
