package Exceptions;

public class EnemyNotFound extends Exception {
    public EnemyNotFound() {
        this("Exception - Enemy not found");
    }
    public EnemyNotFound(String message) {
        super(message);
    }
}
