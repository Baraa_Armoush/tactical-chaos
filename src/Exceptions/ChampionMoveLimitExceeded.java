package Exceptions;

import java.io.Serializable;

public class ChampionMoveLimitExceeded extends Exception {
    public ChampionMoveLimitExceeded() {
        this("Exception - Champion move limit exceeded");
    }
    public ChampionMoveLimitExceeded(String message) {
        super(message);
    }
}
