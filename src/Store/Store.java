package Store;

import Champion.ChampionFactory;
import Champion.ChampionList;
import Champion.ChampionName;
import Filter.Filter;
import Filter.Filterable;

import java.io.Serializable;

public class Store implements Filterable {
    private ChampionList champions;

    public Store() {
        this(new ChampionList());
    }
    public Store(ChampionList champions) {
        this.champions = champions;
    }
    public Store(int championsNumber) {
        this();
        generateRandomChampion(championsNumber);
    }

    public ChampionList getChampions() {
        return champions;
    }
    public void setChampions(ChampionList champions) {
        this.champions = champions;
    }

    public void generateRandomChampion(int championsNumber) {
        for(int i = 0; i < championsNumber; i++) {
            champions.add(ChampionFactory.Create(ChampionName.getRandomChampionName()));
        }
    }

    @Override
    public Store AcceptFilter(Filter filter) {
        return new Store(champions.AcceptFilter(filter));
    }
}
