package Arena;

import Champion.Champion;
import Champion.ChampionList;
import Item.Item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Square {
    private int x, y;
    private boolean viewState;
    private List<Item> items;
    private SquareType squareType;
    private transient ChampionList currentChampions;

    public Square() {
        this(0,0, false, SquareType.Standard, new ChampionList());
    }
    public Square(int x, int y) {
        this(x, y, false, SquareType.Standard, new ChampionList());
    }
    public Square(int x, int y, boolean viewState, SquareType squareType, ChampionList currentChampions) {
        this.x = x;
        this.y = y;
        this.viewState = viewState;
        this.squareType = squareType;
        this.currentChampions = currentChampions;
        this.items = new ArrayList<Item>();
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public boolean isViewState() {
        return viewState;
    }
    public SquareType getSquareType() {
        return squareType;
    }
    public ChampionList getCurrentChampions() {
        return currentChampions;
    }
    public List<Item> getItems() {
        return items;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public void setViewState(boolean viewState) {
        this.viewState = viewState;
    }
    public void setCurrentChampions(ChampionList currentChampions) {
        this.currentChampions = currentChampions;
    }
    public void setSquareType(SquareType squareType) {
        this.squareType = squareType;
    }
    public void setItems(List<Item> items) {
        this.items = items;
    }

    boolean inRange(int n, int m) {
        return 1 <= x && x <= n && 1 <= y && y <= m;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
    public String toStringInArena() {
        String Type = "";
        switch (squareType) {
            case Grass:     Type = "g"; break;
            case Water:     Type = "w"; break;
            case Terrain:   Type = "t"; break;
            case Standard:  Type = "s"; break;
            default:                    break;
        }
        Type += getItems().size();

        String Result = (viewState ? Type : "");
        for(Champion champion : currentChampions.getList()) {
            if(!Result.isEmpty() && !Result.equals(Type)) {
                Result += ",";
            }
            Result += champion.toStringInArena();
        }
        if(currentChampions.getList().isEmpty()) {
            Result += "####";
        }
        return Result;
    }
}
