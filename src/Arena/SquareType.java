package Arena;

import RandomGenerator.RandomGenerator;

public enum SquareType {
    Standard,
    Grass,
    Water,
    Terrain;

    public static SquareType getRandomSquareType(int isBlock) {
        int index = RandomGenerator.getRandomNumber(1 + isBlock, 10);
        if (index > 5)      index = 0;
        else if (index > 3) index = 1;
        else if (index > 1) index = 2;
        else                index = 3;

        return values()[index];
    }
}
