package Arena;

import Item.Item;
import Item.ItemName;
import RandomGenerator.RandomGenerator;

import java.io.Serializable;
import java.util.List;

public class Arena {
    private int n, m;
    private Square[][] squares;

    public Arena() {
        this(10, 10);
    }
    public Arena(int n, int m) {
        this.n = n;
        this.m = m;
        squares = new Square[n][m];
        for(int x = 0; x < n; x++) {
            for(int y = 0; y < m; y++) {
                squares[x][y] = new Square(x + 1, y + 1);
            }
        }
    }

    public int getN() {
        return n;
    }
    public int getM() {
        return m;
    }
    public Square[][] getSquares() {
        return squares;
    }
    public Square getSquare(int x, int y) {
        return squares[x - 1][y - 1];
    }

    public void setN(int n) {
        this.n = n;
    }
    public void setM(int m) {
        this.m = m;
    }
    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }
    public void setSquare(int x, int y, Square square) {
        squares[x - 1][y - 1] = square;
    }

    public void generateRandomSquareType() {
        for (int x = 1; x <= n; x++)
            for (int y = 1; y <= m; y++) {
                int isBlocked = (getSquare(x, y).getCurrentChampions().getList().size() != 0 ? 1 : 0);
                getSquare(x, y).setSquareType(SquareType.getRandomSquareType(isBlocked));
                getSquare(x, y).setViewState(true);
            }
    }
    public void generateRandomItems(int numberOfItems){
        for (int i = 0; i < numberOfItems; i++) {
            int x = RandomGenerator.getRandomNumber(1, getN());
            int y = RandomGenerator.getRandomNumber(1, getM());
            Item item = new Item();
            item.setName(ItemName.getRandomItemName());
            item.setPosition( getSquare(x, y) );
            item.setCurrentChampion(null);
            getSquare(x, y).getItems().add(item);
        }
    }
    public void DistributeItems(List<Item> items) {
        for (Item item : items) {
            int x = RandomGenerator.getRandomNumber(1, getN());
            int y = RandomGenerator.getRandomNumber(1, getM());
            item.setCurrentChampion(null);
            item.setPosition(getSquare(x, y));
            getSquare(x, y).getItems().add(item);
        }
    }
    public boolean isValidSquare(int x, int y) {
        return inRange(x, y) && getSquare(x, y).getSquareType() != SquareType.Terrain;
    }
    public boolean inRange(int x, int y) {
        return inRange(new Square(x, y));
    }
    public boolean inRange(Square square) {
        return square.inRange(n, m);
    }
}
