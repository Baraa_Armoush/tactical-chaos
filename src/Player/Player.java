package Player;

import Arena.Square;
import Champion.Champion;
import Champion.ChampionName;
import Champion.ChampionList;
import Champion.ChampionFactory;
import Filter.ChampionNameAndLevelFilter;
import Game.Game;
import Move.*;
import Store.Store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Player implements Runnable {
    protected int ID;
    protected int budget;
    protected transient Game game;
    protected ChampionList arenaChampions;
    protected ChampionList benchChampions;
    protected transient List<Move> currentMoves;
    private String playerType;


    public Player(String playerType) {
        this(0, 10, null, new ChampionList(), new ChampionList(), new ArrayList<Move>(), playerType);
    }
    public Player(int ID, int budget, Game game, String playerType) {
        this(ID, budget, game, new ChampionList(), new ChampionList(), new ArrayList<Move>(), playerType);
    }
    public Player(int ID, int budget, Game game, ChampionList arenaChampions, ChampionList benchChampions, List<Move> currentMoves, String playerType) {
        this.ID = ID;
        this.budget = budget;
        this.game = game;
        this.arenaChampions = arenaChampions;
        this.benchChampions = benchChampions;
        this.currentMoves = currentMoves;
        this.playerType = playerType;
    }

    public int getID() {
        return ID;
    }
    public int getBudget() {
        return budget;
    }
    public Game getGame() {
        return game;
    }
    public ChampionList getArenaChampions() {
        return arenaChampions;
    }
    public ChampionList getBenchChampions() {
        return benchChampions;
    }
    public List<Move> getCurrentMoves() {
        return currentMoves;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    public void setBudget(int budget) {
        this.budget = budget;
    }
    public void setGame(Game game) {
        this.game = game;
    }
    public void setArenaChampions(ChampionList arenaChampions) {
        this.arenaChampions = arenaChampions;
    }
    public void setBenchChampions(ChampionList benchChampions) {
        this.benchChampions = benchChampions;
    }
    public void setCurrentMoves(List<Move> currentMoves) {
        this.currentMoves = currentMoves;
    }

    public abstract void Display();
    public abstract void Display(String message);
    public abstract void ExceptionPrinter(Exception exception);

    public abstract <Type> Type Choose(String message, List<Type> choices);
    public abstract Square ChoosePosition();
    public abstract Square ChooseSquare();

    @Override
    public void run() {
        ExecuteMoves();
    }

    public boolean PropagateMove(List<MoveType> availableArenaChampionsMoves,
                                 List<MoveType> availableBenchChampionsMoves,
                                 List<MoveType> availableStoreChampionsMoves,
                                 Store temporalStore) {

        ChampionType championType = Choose("Champion type", Arrays.asList(ChampionType.values()));

        if(championType == ChampionType.InArena) {
            Champion champion = Choose("Champion", arenaChampions.getList());
            if(champion != null) {
                MoveType moveType = Choose("Champion move type", availableArenaChampionsMoves);
                if(moveType != null) {
                    switch (moveType) {
                        case Walk:
                            WalkDirection walkDirection = Choose("Walk direction", Arrays.asList(WalkDirection.values()));
                            if(walkDirection != null) {
                                currentMoves.add(new WalkMove(this, champion, walkDirection));
                            }
                            break;
                        case Sell:
                            currentMoves.add(new SellMove(this, champion));
                            break;
                        case BasicAttack:
                            Champion enemy = Choose("Enemy", champion.getEnemiesInVisionRange());
                            if(enemy != null) {
                                currentMoves.add(new BasicAttackMove(this, champion, enemy));
                            }
                            break;
                    }
                }
            }
        } else if(championType == ChampionType.InBench) {
            Champion champion = Choose("Champion", benchChampions.getList());
            if(champion != null) {
                MoveType moveType = Choose("Champion move type", availableBenchChampionsMoves);
                if(moveType != null) {
                    switch (moveType) {
                        case Sell:
                            currentMoves.add(new SellMove(this, champion));
                            break;
                        case Swap:
                            Champion arenaChampion = Choose("Arena Champion", arenaChampions.getList());
                            if(arenaChampion != null) {
                                currentMoves.add(new SwapMove(this, arenaChampion, champion));
                            }
                            break;
                    }
                }
            }
        } else if(championType == ChampionType.InStore) {
            Champion champion = Choose("Champion", temporalStore.getChampions().getList());
            if(champion != null) {
                MoveType moveType = Choose("Champion move type", availableStoreChampionsMoves);
                if(moveType != null) {
                    switch (moveType) {
                        case Buy:
                            currentMoves.add(new BuyMove(this, champion, ChoosePosition()));
                            break;
                        case BuyForFree:
                            currentMoves.add(new BuyForFreeMove(this, champion, ChoosePosition()));
                            break;
                    }
                }
            }
        } else {
            return false;
        }

        return true;
    }

    public void ExecuteMoves() {
        for(Move move : currentMoves) {
            Display(move.toString());
            try {
                move.PerformMove();
                Display("Done successfully");
            } catch (Exception exception) {
                ExceptionPrinter(exception);
            } finally {
                try {
                    Thread.sleep(1500);
                } catch (Exception ignored) {

                }
            }
        }
        currentMoves.clear();
    }

    public void Merge(List<Champion> champions) {
        int x = 0, y = 0;
        int stunned = 0;
        int silenced = 0;
        double health = 0.0;
        double mana = 0.0;
        for(Champion champion : champions) {
            x += champion.getPosition().getX();
            y += champion.getPosition().getY();
            stunned += champion.getStunnedRounds();
            silenced += champion.getSilencedRounds();
            health += champion.getAttributes().getHealth();
            mana += champion.getAttributes().getCurrentMana();
        }
        Champion newChampion = ChampionFactory.Create(champions.get(0).getName());
        newChampion.setCurrentPlayer(this);
        newChampion.setLevel(champions.get(0).getLevel() + 1);
        newChampion.setStunnedRounds(stunned / champions.size());
        newChampion.setSilencedRounds(silenced / champions.size());
        newChampion.getAttributes().setHealth(health / champions.size());
        newChampion.getAttributes().setCurrentMana(mana / champions.size());
        newChampion.getAttributes().setCost(2 * champions.get(0).getAttributes().getCost());
        newChampion.setPosition(game.getArena().getSquare(x / champions.size(), y / champions.size()));
        game.getArena().getSquare(x / champions.size(), y / champions.size()).getCurrentChampions().add(newChampion);
        arenaChampions.add(newChampion);
        for(Champion champion : champions) {
            champion.remove();
        }
    }
    public void CheckPromote(ChampionName championName) {
        for(int level = 1; level <= 2; level++) {
            ChampionList champions = arenaChampions.AcceptFilter(new ChampionNameAndLevelFilter(championName, level));
            while (champions.getList().size() >= 3) {
                List<Champion> mergedChampions = new ArrayList<Champion>();
                for (int i = 0; i < 3; i++) {
                    mergedChampions.add(champions.getList().get(i));
                }
                for (Champion champion : mergedChampions) {
                    champions.remove(champion);
                }
                Merge(mergedChampions);
            }
        }
    }

    @Override
    public String toString() {
        return "Player" + ID + " : " + "\r\n" +
                    "Budget : " + budget + "\r\n" +
                        "Arena champions : \r\n" + arenaChampions + "\r\n" +
                            "Bench champions : \r\n" + benchChampions;
    }

    enum ChampionType {
        InArena,
        InBench,
        InStore;

        @Override
        public String toString() {
            switch (this) {
                case InArena: return "Arena champion";
                case InBench: return "Bench champion";
                case InStore: return "Store champion";
                default:      return null;
            }
        }
    }
}
