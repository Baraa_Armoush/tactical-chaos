package Player;

import Arena.Arena;
import Arena.Square;
import Champion.ChampionList;
import Game.Game;
import Move.Move;
import RandomGenerator.RandomGenerator;

import java.io.Serializable;
import java.util.List;

public class AutoPlayer extends Player {

    public AutoPlayer() {
        super("autoPlayer");
    }
    public AutoPlayer(int ID, int budget, Game game, String playerType) {
        super(ID, budget, game, playerType);
    }
    public AutoPlayer(int ID, int budget, Game game, ChampionList arenaChampions, ChampionList benchChampions, List<Move> currentMoves, String playerType) {
        super(ID, budget, game, arenaChampions, benchChampions, currentMoves, playerType);
    }

    @Override
    public void Display() {
        Display(this.toString());
    }

    @Override
    public void Display(String message) {
        System.out.println(message);
    }

    @Override
    public void ExceptionPrinter(Exception exception) {
        System.out.println(exception.getMessage());
    }

    @Override
    public <Type> Type Choose(String message, List<Type> choices) {
        try {
            System.out.println("Chose " + message + ", Enter : ");
            for (Type choice : choices) {
                System.out.println("\t" + choices.indexOf(choice) + " - " + choice);
            }
            System.out.println("\t" + choices.size() + " - Exit");
            System.out.print("Enter : ");
            int x = RandomGenerator.getRandomNumber(0, choices.size());
            Thread.sleep(1000);
            System.out.print(x);
            Thread.sleep(1000);
            System.out.println();
            if (x == choices.size()) {
                return null;
            }
            return choices.get(x);
        } catch (Exception exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return Choose(message, choices);
        }
    }

    @Override
    public Square ChooseSquare() {
        Arena arena = game.getArena();
        try {
            System.out.println("Chose square , Enter : ");
            System.out.print("x : ");
            Thread.sleep(1000);
            int X = RandomGenerator.getRandomNumber(1, arena.getN());
            System.out.print(X);
            Thread.sleep(1000);
            System.out.println();
            System.out.print("y : ");
            Thread.sleep(1000);
            int Y = RandomGenerator.getRandomNumber(1, arena.getM());
            System.out.print(Y);
            Thread.sleep(1000);
            System.out.println();
            return arena.getSquare(X, Y);
        } catch (Exception exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return ChooseSquare();
        }
    }

    @Override
    public Square ChoosePosition() {
        try {
            System.out.println("Chose position , Enter : ");
            System.out.println("\t0 - in Arena");
            System.out.println("\t1 - in Bench");
            System.out.print("Enter : ");
            Thread.sleep(1000);
            int x = RandomGenerator.getRandomNumber(0, 1);
            System.out.print(x);
            Thread.sleep(1000);
            System.out.println();
            if(x == 0) {
                return ChooseSquare();
            } else {
                return null;
            }
        } catch (Exception exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return ChoosePosition();
        }
    }
}
