package Player;

import Arena.Arena;
import Arena.Square;
import Exceptions.IllegalSquare;
import Exceptions.InvalidInput;
import Game.Game;
import Move.Move;
import Champion.ChampionList;

import java.io.Serializable;
import java.util.List;
import java.util.Scanner;

public class ConsolePlayer extends Player {

    public ConsolePlayer() {
        super("consolePlayer");
    }
    public ConsolePlayer(int ID, int budget, Game game, String playerType) {
        super(ID, budget, game, playerType);
    }
    public ConsolePlayer(int ID, int budget, Game game, ChampionList arenaChampions, ChampionList benchChampions, List<Move> currentMoves, String playerType) {
        super(ID, budget, game, arenaChampions, benchChampions, currentMoves, playerType);
    }

    @Override
    public void Display() {
        Display(this.toString());
    }

    @Override
    public void Display(String message) {
        System.out.println(message);
    }

    @Override
    public void ExceptionPrinter(Exception exception) {
        System.out.println(exception.getMessage());
    }

    @Override
    public <Type> Type Choose(String message, List<Type> choices) {
        try {
            System.out.println("Chose " + message + ", Enter : ");
            for (Type choice : choices) {
                System.out.println("\t" + choices.indexOf(choice) + " - " + choice);
            }
            System.out.println("\t" + choices.size() + " - Exit");
            Scanner Reader = new Scanner(System.in);
            System.out.print("Enter : ");
            String x = Reader.nextLine();
            if (((Integer) (choices.size())).toString().equals(x)) {
                return null;
            }
            for (Type choice : choices) {
                if (((Integer) (choices.indexOf(choice))).toString().equals(x)) {
                    return choice;
                }
            }
            throw new InvalidInput();
        } catch (InvalidInput exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return Choose(message, choices);
        }
    }

    @Override
    public Square ChooseSquare() {
        Arena arena = game.getArena();
        try {
            System.out.println("Chose square , Enter : ");
            Scanner Reader = new Scanner(System.in);
            System.out.print("x : ");   String x = Reader.nextLine();
            System.out.print("y : ");   String y = Reader.nextLine();
            int X, Y;
            try {
                X = Integer.parseInt(x);
                Y = Integer.parseInt(y);
            } catch (Exception exception) {
                throw new InvalidInput();
            }
            if(!arena.inRange(X, Y)) {
                throw new IllegalSquare();
            }
            return arena.getSquare(X, Y);
        } catch (Exception exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return ChooseSquare();
        }
    }

    @Override
    public Square ChoosePosition() {
        try {
            System.out.println("Chose position , Enter : ");
            System.out.println("\t0 - in Arena");
            System.out.println("\t1 - in Bench");
            Scanner Reader = new Scanner(System.in);
            System.out.print("Enter : "); String x = Reader.nextLine();
            if(x.equals("0")) {
                return ChooseSquare();
            } else if(x.equals("1")) {
                return null;
            } else {
                throw new InvalidInput();
            }
        } catch (Exception exception) {
            ExceptionPrinter(exception);
            System.out.println("Repeat : ");
            return ChoosePosition();
        }
    }
}
