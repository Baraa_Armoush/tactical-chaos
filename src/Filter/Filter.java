package Filter;

public abstract class Filter {
    public abstract boolean isAcceptable(Object object);
}
