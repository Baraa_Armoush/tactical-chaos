package Filter;

import Champion.Champion;
import Champion.ChampionList;

import java.util.ArrayList;
import java.util.Collections;

public class TemporalStoreFilter extends Filter {
    private int championsNumber;

    public TemporalStoreFilter() {
        this(5);
    }
    public TemporalStoreFilter(int championsNumber) {
        this.championsNumber = championsNumber;
    }

    @Override
    public boolean isAcceptable(Object object) {
        return true;
    }

    public void DoShuffleAndRandomFilter(ChampionList championList) {
        Collections.shuffle(championList.getList());
        championList.setList(new ArrayList<Champion>(championList.getList().subList(0, Math.min(championsNumber, championList.getList().size()))));
    }
}
