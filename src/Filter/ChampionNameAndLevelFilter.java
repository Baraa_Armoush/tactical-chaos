package Filter;

import Champion.Champion;
import Champion.ChampionName;

public class ChampionNameAndLevelFilter extends Filter {
    private ChampionName championName;
    private int Level;

    public ChampionNameAndLevelFilter() {
        this(null, 0);
    }
    public ChampionNameAndLevelFilter(ChampionName championName, int level) {
        this.championName = championName;
        Level = level;
    }

    public ChampionName getChampionName() {
        return championName;
    }
    public int getLevel() {
        return Level;
    }

    public void setChampionName(ChampionName championName) {
        this.championName = championName;
    }
    public void setLevel(int level) {
        Level = level;
    }

    @Override
    public boolean isAcceptable(Object object) {
        return ((Champion) object).getName() == championName && ((Champion) object).getLevel() == Level;
    }
}
