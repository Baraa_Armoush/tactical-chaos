package Filter;

public interface Filterable {
    public Filterable AcceptFilter(Filter filter);
}
