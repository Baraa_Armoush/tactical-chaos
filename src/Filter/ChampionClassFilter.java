package Filter;

import Champion.Champion;
import Champion.ChampionClass;

public class ChampionClassFilter extends Filter {
    private ChampionClass championClass;

    public ChampionClassFilter() {
        this(ChampionClass.None);
    }
    public ChampionClassFilter(ChampionClass championClass) {
        this.championClass = championClass;
    }

    @Override
    public boolean isAcceptable(Object object) {
        for (int i = 0; i < ((Champion)object).getActiveClasses().length; i++) {
            if (((Champion)object).getActiveClasses()[i].equals(championClass)) {
                return true;
            }
        }
        return false;
    }
}
