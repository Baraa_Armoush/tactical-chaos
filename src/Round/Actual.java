package Round;

import Phase.Executing;
import Phase.Planning;

import java.io.Serializable;

public class Actual extends Round { // after first nine rounds
    public Actual() {
        super("actual");
        phases.add(new Planning());
        phases.add(new Executing());
    }

    @Override
    public String toString() {
        return "Actual Round";
    }
}
