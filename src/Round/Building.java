package Round;

import Phase.Executing;
import Phase.Preparation;

import java.io.Serializable;

public class Building extends Round {  // first nine rounds
    public Building() {
        super("building");
        phases.add(new Preparation());
        phases.add(new Executing());
    }

    @Override
    public String toString() {
        return "Building Round";
    }
}
