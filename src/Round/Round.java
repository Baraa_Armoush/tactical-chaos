package Round;

import Champion.Champion;
import Game.Game;
import Phase.Phase;
import Player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Round {
    protected List<Phase> phases;
    private String roundType;

    public Round(String roundType) {
        this(new ArrayList<Phase>(), roundType);
    }
    public Round(List<Phase> phases, String roundType) {
        this.phases = phases;
        this.roundType = roundType;
    }

    public List<Phase> getPhases() {
        return phases;
    }
    public void setPhases(List<Phase> phases) {
        this.phases = phases;
    }

    public void Run(Game game) {
        for(Player player : game.getCurrentPlayers()) {
            player.setBudget(player.getBudget() + game.getConstrains().getGivenCoinsEachRound());
            for (Champion champion : player.getArenaChampions().getList())
                champion.setNumberOfMove(0);
            for (Champion champion : player.getBenchChampions().getList())
                champion.setNumberOfMove(0);
        }

        for(Phase phase : phases) {
            System.out.println("\t" + phase + " : ");
            phase.Run(game);
            System.out.println("\t------------");
        }
        System.out.println("-----------------------------------------");
    }
}
