package Round;

import Game.Game;

import java.io.Serializable;
import java.util.Queue;

public class RoundManager {
    private transient Game game;
    private int roundNumber = 1;
    private Queue<Round> rounds;

    public RoundManager() {
    }
    public RoundManager(Game game, Queue<Round> rounds) {
        this.game = game;
        this.rounds = rounds;
    }

    public Game getGame() {
        return game;
    }
    public Queue<Round> getRounds() {
        return rounds;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    public void setRounds(Queue<Round> rounds) {
        this.rounds = rounds;
    }

    public boolean hasRounds() {
        return !rounds.isEmpty();
    }
    public void AddRound(Round round) {
        rounds.add(round);
    }
    public void RunCurrentRound() {
        rounds.remove().Run(game);
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }
}
