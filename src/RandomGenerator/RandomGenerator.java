package RandomGenerator;

import java.io.Serializable;

public class RandomGenerator {
    public static int getRandomNumber(int L, int R) {
        int x = (int) (Math.random() * 1000000000);
        return L + x % (R - L + 1);
    }
}
