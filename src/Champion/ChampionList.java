package Champion;

import Filter.Filter;
import Filter.Filterable;
import Filter.TemporalStoreFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChampionList implements Filterable {
    private List<Champion> champions;

    public ChampionList() {
        this(new ArrayList <Champion>());
    }
    public ChampionList(List<Champion> champions) {
        this.champions = champions;
    }

    public List<Champion> getList() {
        return champions;
    }
    public void setList(List<Champion> champions) {
        this.champions = champions;
    }

    public void add(Champion champion) {
        champions.add(champion);
    }
    public void remove(Champion champion) {
        champions.remove(champion);
    }

    @Override
    public ChampionList AcceptFilter(Filter filter) {
        ChampionList Result = new ChampionList();
        for(Champion champion : champions) {
            if(filter.isAcceptable(champion)) {
                Result.add(champion);
            }
        }
        if(filter instanceof TemporalStoreFilter) {
            ((TemporalStoreFilter)filter).DoShuffleAndRandomFilter(Result);
        }
        return Result;
    }

    @Override
    public String toString() {
        String Result = "";
        for(Champion champion : champions) {
            Result += champion + "\r\n";
        }
        if(champions.isEmpty()) {
            Result += "Empty";
        }
        return Result;
    }
}
