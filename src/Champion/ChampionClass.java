package Champion;

import java.io.Serializable;

public enum ChampionClass {
    Assassin,
    BladeMaster,
    Brawler,
    Demon,
    Dragons,
    Elementalist,
    Glacial,
    Gunslinger,
    Imperial,
    Knight,
    Ninja,
    Noble,
    Pirate,
    Ranger,
    ShapeShifter,
    Sorcerer,
    Void,
    Wild,
    Yordle,
    None
}
