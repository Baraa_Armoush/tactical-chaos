package Champion;

import RandomGenerator.RandomGenerator;

import java.io.Serializable;

public enum ChampionName {
    Aatrox,
    Brand,
    Chogath,
    Darius,
    Evelynn,
    Fiora,
    Gankplank,
    Karthus,
    Leona,
    MissFortune,
    Nidale,
    Poppy,
    Ranger,
    Sejuani,
    Talon;

    public static ChampionName getRandomChampionName() {
        //int index = RandomGenerator.getRandomNumber(0, 1);
        int index = RandomGenerator.getRandomNumber(0, values().length - 1);
        return values()[index];
    }
}
