package Champion;

import Arena.Arena;
import Arena.Square;
import Arena.SquareType;
import Item.ItemName;
import Damage.Damage;
import Item.Item;
import Player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Champion {
    private ChampionName name;
    private List<Item> items;
    private transient Player currentPlayer;
    private ChampionClass[] activeClasses;
    private ChampionAttributes attributes;
    private Square position;
    private int level;
    private int stunnedRounds;
    private int silencedRounds;
    private int numberOfMove;

    public Champion() {
        this(null,null, new ChampionClass[3], new ChampionAttributes(),
                null, 1, 0,0);
    }
    public Champion(ChampionName name, Player currentPlayer, ChampionClass[] activeClasses, ChampionAttributes attributes,
                    Square position, int level, int stunnedRounds, int silencedRounds) {
        this.name = name;
        this.currentPlayer = currentPlayer;
        this.activeClasses = activeClasses;
        this.attributes = attributes;
        this.position = position;
        this.level = level;
        this.stunnedRounds = stunnedRounds;
        this.silencedRounds = silencedRounds;
        this.numberOfMove = 0;
        this.items = new ArrayList<Item>();
    }

    public ChampionName getName() {
        return name;
    }
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    public ChampionClass[] getActiveClasses() {
        return activeClasses;
    }
    public ChampionAttributes getAttributes() {
        return attributes;
    }
    public Square getPosition() {
        return position;
    }
    public int getLevel() {
        return level;
    }
    public int getStunnedRounds() {
        return stunnedRounds;
    }
    public int getSilencedRounds() {
        return silencedRounds;
    }
    public int getNumberOfMove() {
        return numberOfMove;
    }
    public List<Item> getItems() {
        return items;
    }

    public int getVisionRange() {
        int visionRange = getAttributes().getVisionRange();
        if (this.position.getSquareType() == SquareType.Grass)
            visionRange /= 2;
        return visionRange;
    }
    public int getMovementSpeed() {
        int movementSpeed = getAttributes().getMovementSpeed();
        if (this.position.getSquareType() == SquareType.Water)
            movementSpeed /= 2;
        return movementSpeed;
    }
    public double getArmor() {
        double armor = getAttributes().getArmor();
        boolean isContain = false;
        for (Item item : items)
            isContain |= (item.getName() == ItemName.KnightArmor);
        if (isContain) armor *= 1.15;
        return armor;
    }
    public double getAttackDamage() {
        double attackDamage = getAttributes().getAttackDamage();
        double Added = 0;
        for (Item item : items)
            switch (item.getName()) {
                case MagicHat:      Added += 20;    break;
                case WarriorGloves: Added += 10;    break;
                case NightShift:    Added += 20;    break;
            }
        return attackDamage * (1 + Added / 100);
    }
    public double getCriticalStrikeChance() {
        double criticalStrikeChance = getAttributes().getCriticalStrikeChance();
        boolean isContain = false;
        for (Item item : items)
            isContain |= (item.getName() == ItemName.AngryCloak);
        if (isContain) criticalStrikeChance *= 1.1;
        return  criticalStrikeChance;
    }
    public double getMaxHealth() {
        double maxHealth = getAttributes().getMaxHealth();
        boolean isContain = false;
        for (Item item : items)
            isContain |= (item.getName() == ItemName.VoidHit);
        if (isContain) maxHealth *= 1.05;
        return  maxHealth;
    }
    public double getMagicResist() {
        double magicResist = getAttributes().getMagicResist();
        boolean isContain = false;
        for (Item item : items)
            isContain |= (item.getName() == ItemName.UniverseCore);
        if (isContain) magicResist *= 1.2;
        return  magicResist;
    }


    public void setName(ChampionName name) {
        this.name = name;
    }
    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
    public void setActiveClasses(ChampionClass[] activeClasses) {
        this.activeClasses = activeClasses;
    }
    public void setAttributes(ChampionAttributes attributes) {
        this.attributes = attributes;
    }
    public void setPosition(Square position) {
        this.position = position;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public void setStunnedRounds(int stunnedRounds) {
        this.stunnedRounds = stunnedRounds;
    }
    public void setSilencedRounds(int silencedRounds) {
        this.silencedRounds = silencedRounds;
    }
    public void setNumberOfMove(int numberOfMove) {
        this.numberOfMove = numberOfMove;
    }
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public int Distance(Champion champion) {
        return Math.abs(this.getPosition().getX() - champion.getPosition().getX())
                + Math.abs(this.getPosition().getY() - champion.getPosition().getY());
    }
    public boolean isInVisionRange(Champion champion) {
        return this.Distance(champion) <= this.getVisionRange();
    }
    public boolean isInAttackRange(Champion champion) {
        return this.Distance(champion) <= this.getAttributes().getAttackRange();
    }
    public List<Champion> getEnemiesInVisionRange() {
        Arena arena = getCurrentPlayer().getGame().getArena();
        List<Champion> enemies = new ArrayList<Champion>();
        for(int x = 1; x <= arena.getN(); x++) {
            for(int y = 1; y <= arena.getM(); y++) {
                for(Champion enemy : arena.getSquare(x, y).getCurrentChampions().getList()) {
                    if(enemy.getCurrentPlayer() != currentPlayer) {
                        if (isInVisionRange(enemy)) {
                            enemies.add(enemy);
                        }
                    }
                }
            }
        }
        return enemies;
    }

    public void EquipItems() {
        while (getItems().size() < 3) {
            if (getPosition().getItems().size() == 0)
                break;
            getItems().add( getPosition().getItems().get(0) );
            getPosition().getItems().remove(0);
        }
    }

    public void GetIntendedDamage(Damage damage) {
        damage.ApplyDamage(this);
        if(attributes.getHealth() <= 0.000001) {
            getCurrentPlayer().getGame().getArena().DistributeItems(getItems());
            remove();
        }
    }
    public void remove() {
        position.getCurrentChampions().remove(this);
        currentPlayer.getArenaChampions().remove(this);
        currentPlayer.getBenchChampions().remove(this);
        currentPlayer = null;
        position = null;
    }

    public double generateBasicAttackDamage() {
        return (Math.random() < getCriticalStrikeChance() ? attributes.getCriticalStrikeDamage() : 1.0) * getAttackDamage();
    }

    @Override
    public String toString() {
        String Result = name + " : " + "position : " + position + ", Level : " + level +
                            ", health : " + attributes.getHealth() + "/" + getMaxHealth() +
                                ", Mana : " + attributes.getCurrentMana() + "/" + attributes.getMaxMana() +
                                    ", Cost : " + attributes.getCost();

        if (stunnedRounds > 0) {
            Result += ", STUNNED for the next " + stunnedRounds + "rounds";
        } if (silencedRounds > 0) {
            Result += ", STUNNED for the next " + stunnedRounds + "rounds";
        }

        Result += ".";

        return Result;
    }
    public String toStringInArena() {
        return name.toString().substring(0, 2) + "" + currentPlayer.getID() + "" + level;
    }

    public static class ChampionAttributes {
        private int cost;
        private final double maxHealth;
        private double health;
        private double armor;
        private double magicResist;
        private int visionRange;
        private int attackRange;
        private double attackDamage;
        private int movementSpeed;
        private double criticalStrikeChance;
        private double criticalStrikeDamage;
        private final double maxMana;
        private double currentMana;
        private double manaStart;
        private double manaCost;

        public ChampionAttributes() {
            this(0,0,0,0.0,0.0,0,0,0,0,0.0,0.0,0,0,0);
        }
        public ChampionAttributes(int cost, double maxHealth, double health, double armor, double magicResist,
                                    int visionRange, int attackRange, double attackDamage, int movementSpeed,
                                        double criticalStrikeChance, double criticalStrikeDamage,
                                            double maxMana, double manaStart, double manaCost) {
            this.cost = cost;
            this.maxHealth = maxHealth;
            this.health = health;
            this.armor = armor;
            this.magicResist = magicResist;
            this.visionRange = visionRange;
            this.attackRange = attackRange;
            this.attackDamage = attackDamage;
            this.movementSpeed = movementSpeed;
            this.criticalStrikeChance = criticalStrikeChance;
            this.criticalStrikeDamage = criticalStrikeDamage;
            this.maxMana = maxMana;
            this.currentMana = this.manaStart = manaStart;
            this.manaCost = manaCost;
        }

        public int getCost() {
            return cost;
        }
        public double getMaxHealth() {
            return maxHealth;
        }
        public double getHealth() {
            return health;
        }
        public double getArmor() {
            return armor;
        }
        public double getMagicResist() {
            return magicResist;
        }
        public int getVisionRange() {
            return visionRange;
        }
        public int getAttackRange() {
            return attackRange;
        }
        public double getAttackDamage() {
            return attackDamage;
        }
        public int getMovementSpeed() {
            return movementSpeed;
        }
        public double getCriticalStrikeChance() {
            return criticalStrikeChance;
        }
        public double getCriticalStrikeDamage() {
            return criticalStrikeDamage;
        }
        public double getMaxMana() {
            return maxMana;
        }
        public double getCurrentMana() {
            return currentMana;
        }
        public double getManaStart() {
            return manaStart;
        }
        public double getManaCost() {
            return manaCost;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }
        public void setHealth(double health) {
            this.health = health;
        }
        public void setArmor(double armor) {
            this.armor = armor;
        }
        public void setMagicResist(double magicResist) {
            this.magicResist = magicResist;
        }
        public void setVisionRange(int visionRange) {
            this.visionRange = visionRange;
        }
        public void setAttackRange(int attackRange) {
            this.attackRange = attackRange;
        }
        public void setAttackDamage(double attackDamage) {
            this.attackDamage = attackDamage;
        }
        public void setMovementSpeed(int movementSpeed) {
            this.movementSpeed = movementSpeed;
        }
        public void setCriticalStrikeChance(double criticalStrikeChance) {
            this.criticalStrikeChance = criticalStrikeChance;
        }
        public void setCriticalStrikeDamage(double criticalStrikeDamage) {
            this.criticalStrikeDamage = criticalStrikeDamage;
        }
        public void setCurrentMana(double currentMana) {
            this.currentMana = currentMana;
        }
        public void setManaStart(double manaStart) {
            this.manaStart = manaStart;
        }
        public void setManaCost(double manaCost) {
            this.manaCost = manaCost;
        }
    }
}
