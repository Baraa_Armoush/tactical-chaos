package Champion;

public class ChampionFactory extends Champion {

    public static Champion Create (ChampionName name) {
        switch (name) {
            case Aatrox:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Demon, ChampionClass.BladeMaster, ChampionClass.None},
                        new ChampionAttributes(3, 750, 750, 0.40, 0.40, 20, 10, 50, 2, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Brand:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Demon, ChampionClass.Sorcerer, ChampionClass.None},
                        new ChampionAttributes(4, 400, 400, 0.30, 0.30, 20, 10, 25, 2, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Chogath:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Void, ChampionClass.Brawler, ChampionClass.None},
                        new ChampionAttributes(1, 600, 600, 0.30, 0.30, 25, 15, 35, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Darius:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Imperial, ChampionClass.Knight, ChampionClass.Brawler},
                        new ChampionAttributes(1, 600, 600, 0.50, 0.50, 20, 10, 50, 2, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Evelynn:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Demon, ChampionClass.Assassin, ChampionClass.None},
                        new ChampionAttributes(3, 500, 500, 0.25, 0.25, 20, 10, 20, 3, 0.25, 1.50, 100, 5, 8),
                        null, 1, 0, 0);
            case Fiora:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Noble, ChampionClass.BladeMaster, ChampionClass.None},
                        new ChampionAttributes(1, 500, 500, 0.40, 0.40, 20, 10, 45, 4, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Gankplank:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Pirate, ChampionClass.BladeMaster, ChampionClass.Gunslinger},
                        new ChampionAttributes(4, 200, 200, 0.40, 0.40, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Karthus:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Void, ChampionClass.Sorcerer, ChampionClass.None},
                        new ChampionAttributes(4, 450, 450, 0.30, 0.30, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Leona:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Noble, ChampionClass.BladeMaster, ChampionClass.Brawler},
                        new ChampionAttributes(2, 500, 500, 0.40, 0.40, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case MissFortune:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Pirate, ChampionClass.Gunslinger, ChampionClass.None},
                        new ChampionAttributes(1, 200, 200, 0.20, 0.20, 60, 30, 50, 2, 0.25, 2.00, 100, 5, 10),
                        null, 1, 0, 0);
            case Nidale:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Wild, ChampionClass.ShapeShifter, ChampionClass.None},
                        new ChampionAttributes(1, 500, 500, 0.45, 0.45, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Poppy:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Yordle, ChampionClass.Knight, ChampionClass.Brawler},
                        new ChampionAttributes(6, 650, 650, 0.50, 0.50, 20, 10, 50, 1, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Ranger:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Wild, ChampionClass.Assassin, ChampionClass.None},
                        new ChampionAttributes(2, 500, 500, 0.25, 0.25, 20, 10, 50, 1, 0.25, 1.50, 100, 5, 7),
                        null, 1, 0, 0);
            case Sejuani:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Glacial, ChampionClass.Knight, ChampionClass.None},
                        new ChampionAttributes(2, 550, 550, 0.50, 0.50, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            case Talon:
                return new Champion(name,null, new ChampionClass[] {ChampionClass.Ninja, ChampionClass.BladeMaster, ChampionClass.Assassin},
                        new ChampionAttributes(6, 500, 500, 0.40, 0.40, 20, 10, 50, 3, 0.25, 1.50, 100, 5, 10),
                        null, 1, 0, 0);
            default:
                return new Champion();
        }
    }
}
