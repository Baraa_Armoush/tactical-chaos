package Adapter;

import Phase.*;
import Player.*;
import Round.*;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

public class AdapterUtils {
    
    public static RuntimeTypeAdapterFactory<Player> getPlayersAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Player.class, "playerType")
                .registerSubtype(AutoPlayer.class, "autoPlayer")
                .registerSubtype(ConsolePlayer.class, "consolePlayer");
    }

    public static RuntimeTypeAdapterFactory<Round> getRoundAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Round.class, "roundType")
                .registerSubtype(Actual.class, "actual")
                .registerSubtype(Building.class, "building");
    }

    public static RuntimeTypeAdapterFactory<Phase> getPhaseAdapter() {
        return RuntimeTypeAdapterFactory
                .of(Phase.class, "phaseType")
                .registerSubtype(Executing.class, "executing")
                .registerSubtype(Planning.class, "planning")
                .registerSubtype(Preparation.class, "preparation");
    }
}
