package Game;

import Round.RoundManager;

public class GameData {
    private Game game;
    private GameConstrains constrains;
    private RoundManager roundManager;

    public GameData(Game game, GameConstrains constrains, RoundManager roundManager) {
        this.game = game;
        this.constrains = constrains;
        this.roundManager = roundManager;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public GameConstrains getConstrains() {
        return constrains;
    }

    public void setConstrains(GameConstrains constrains) {
        this.constrains = constrains;
    }

    public RoundManager getRoundManager() {
        return roundManager;
    }

    public void setRoundManager(RoundManager roundManager) {
        this.roundManager = roundManager;
    }
}

