package Game;

import Arena.Arena;
import Champion.Champion;
import Exceptions.InvalidInput;
import Item.Item;
import Player.Player;
import RandomGenerator.RandomGenerator;
import Round.RoundManager;
import Adapter.AdapterUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.awt.geom.Area;
import java.io.*;
import java.util.Scanner;

public class TacticalChaosTM {
    private Game game;
    private GameConstrains constrains;
    private RoundManager roundManager;

    public TacticalChaosTM() {
        this(new GameConstrains());
    }
    public TacticalChaosTM(GameConstrains constrains) {
        this(new Game(), constrains, new RoundManager());
        game.InitializeGame(constrains, roundManager);
    }
    private TacticalChaosTM(Game game, GameConstrains constrains, RoundManager roundManager) {
        this.game = game;
        this.constrains = constrains;
        this.roundManager = roundManager;
    }

    public Game getGame() {
        return game;
    }
    public GameConstrains getConstrains() {
        return constrains;
    }
    public RoundManager getRoundManager() {
        return roundManager;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    public void setConstrains(GameConstrains constrains) {
        this.constrains = constrains;
    }
    public void setRoundManager(RoundManager roundManager) {
        this.roundManager = roundManager;
    }

    public void StartGame() {
        int roundNumber = roundManager.getRoundNumber();
        while(roundManager.hasRounds()) {
            if (roundNumber == constrains.getBuildingRoundsNumber() + 1) {
                game.getArena().generateRandomSquareType();
                int Min = 20;
                int Max = Math.max(Min, getGame().getArena().getM() * getGame().getArena().getN());
                game.getArena().generateRandomItems(RandomGenerator.getRandomNumber(Min, Max));
            }
            game.DisplayArenaAndPlayersInConsole();
            System.out.println("Round " + roundNumber + " : (" + roundManager.getRounds().element() + ")");
            roundManager.RunCurrentRound();
            roundManager.setRoundNumber(++roundNumber);

            if (BackHome()) {
                ExitGame();
                return;
            }
        }
        game.DisplayArenaAndPlayersInConsole();
    }

    public void ExitGame() {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(new GameData(game, constrains, roundManager));
            BufferedWriter writer = new BufferedWriter(new FileWriter("data\\TacticalChaos.json"));
            writer.write(json);
            writer.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void ResumeGame() {
        try {
            Gson gson = getGsonWithAdapters();
            GameData gameData = gson.fromJson(new FileReader("data\\TacticalChaos.json"), GameData.class);

            game = gameData.getGame();
            constrains = gameData.getConstrains();
            roundManager = gameData.getRoundManager();
            roundManager.setGame(game);

            for (Player player : game.getCurrentPlayers()) {
                player.setGame(game);
                for (Champion champion : player.getArenaChampions().getList()) {
                    champion.setCurrentPlayer(player);
                    champion.getPosition().getCurrentChampions().add(champion);
                    for (Item item : champion.getItems()) {
                        item.setCurrentChampion(champion);
                    }
                }
                for (Champion champion : player.getBenchChampions().getList()) {
                    champion.setCurrentPlayer(player);
                    for (Item item : champion.getItems()) {
                        item.setCurrentChampion(champion);
                    }
                }
            }

            Arena arena = game.getArena();
            for (Player player : game.getCurrentPlayers())
                for (Champion champion : player.getArenaChampions().getList()) {
                    int x = champion.getPosition().getX();
                    int y = champion.getPosition().getY();
                    arena.setSquare(x, y, champion.getPosition());
                }

            for (int x = 1; x <= arena.getN(); x++)
                for (int y = 1; y <= arena.getM(); y++)
                    for (Item item : arena.getSquare(x, y).getItems())
                        item.setPosition(arena.getSquare(x, y));

            StartGame();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Gson getGsonWithAdapters() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(AdapterUtils.getPlayersAdapter())
                .registerTypeAdapterFactory(AdapterUtils.getPhaseAdapter())
                .registerTypeAdapterFactory(AdapterUtils.getRoundAdapter())
                .create();
    }

    public boolean BackHome() {
        try {
            System.out.println("\n\nDo you want to back home?");
            System.out.println("0 - Yes, I want. :(\n"
                    + "1 - No, continue playing :)\n"
                    + "Enter : ");
            Scanner Reader = new Scanner(System.in);
            String x = Reader.nextLine();
            int X;
            try {
                X = Integer.parseInt(x);
            } catch (Exception exception) {
                throw new InvalidInput();
            }
            if (X < 0 || X > 1) {
                throw new InvalidInput();
            }
            return (X == 0);
        }catch (Exception exception) {
            System.out.println(exception.getMessage());
            System.out.println("Repeat");
            return BackHome();
        }
    }
}
