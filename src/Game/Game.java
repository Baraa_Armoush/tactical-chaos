package Game;

import Arena.Arena;
import Player.ConsolePlayer;
import Player.Player;
import Round.Actual;
import Round.Building;
import Round.Round;
import Round.RoundManager;
import Store.Store;
import Player.AutoPlayer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Game {
    private Arena arena;
    private transient Store store;
    private List<Player> currentPlayers;
    GameConstrains constrains;

    public Game() {
        constrains = new GameConstrains();
        store = new Store(constrains.getStoreSize());
    }
    public Game(Arena arena, Store store, List<Player> currentPlayers) {
        this.arena = arena;
        this.store = store;
        this.currentPlayers = currentPlayers;
    }
    public Game(Arena arena, Store store, List<Player> currentPlayers, GameConstrains constrains) {
        this.arena = arena;
        this.store = store;
        this.currentPlayers = currentPlayers;
        this.constrains = constrains;
    }

    public void InitializeGame(GameConstrains constrains, RoundManager roundManager) {
        this.constrains = constrains;
        this.arena = new Arena(constrains.getArenaN(), constrains.getArenaM());
        this.currentPlayers = new ArrayList<Player>(constrains.getPlayersNumber());
        this.store = new Store(constrains.getStoreSize());
        roundManager.setGame(this);
        roundManager.setRoundNumber(1);
        roundManager.setRounds(new LinkedList<Round>());
        for(int i = 0; i < constrains.getPlayersNumber() / 2; i++) {
            currentPlayers.add(new ConsolePlayer(i + 1, constrains.getInitialBudget(), this, "consolePlayer"));
        }
        for(int i = constrains.getPlayersNumber() / 2; i < constrains.getPlayersNumber(); i++) {
            currentPlayers.add(new AutoPlayer(i + 1, constrains.getInitialBudget(), this, "autoPlayer"));
        }
        /*for(int i = 0; i < constrains.getPlayersNumber(); i++) {
            currentPlayers.add(new ConsolePlayer(i + 1, constrains.getInitialBudget(), this));
        }*/
        for(int i = 0; i < constrains.getBuildingRoundsNumber(); i++) {
            roundManager.getRounds().add(new Building());
        }
        for(int i = 0; i < constrains.getActualRoundsNumber(); i++) {
            roundManager.getRounds().add(new Actual());
        }
    }

    public Arena getArena() {
        return arena;
    }
    public Store getStore() {
        return store;
    }
    public List<Player> getCurrentPlayers() {
        return currentPlayers;
    }
    public GameConstrains getConstrains() {
        return constrains;
    }
    public void setArena(Arena arena) {
        this.arena = arena;
    }
    public void setStore(Store store) {
        this.store = store;
    }
    public void setCurrentPlayers(List<Player> currentPlayers) {
        this.currentPlayers = currentPlayers;
    }
    public void setConstrains(GameConstrains constrains) {
        this.constrains = constrains;
    }

    public void DisplayArenaAndPlayersInConsole() {
        System.out.println("Game Arena : ");
        for(int x = 1; x <= arena.getN(); x++) {
            for(int y = 1; y <= arena.getM(); y++) {
                System.out.print(arena.getSquare(x, y).toStringInArena());
                if(y < arena.getM()) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
        }
        for(Player player : currentPlayers) {
            System.out.println(player + "\r\n");
        }
        System.out.println("-------------------------");
    }
}
