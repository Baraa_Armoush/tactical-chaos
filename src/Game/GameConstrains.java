package Game;

import Exceptions.InvalidInput;

import java.io.Serializable;
import java.util.Scanner;

public class GameConstrains {
    private int playersNumber;
    private int buildingRoundsNumber;
    private int actualRoundsNumber;
    private int arenaN;
    private int arenaM;
    private int storeSize;
    private int maxPlayerArenaChampion;
    private int maxPlayerBenchChampion;
    private int initialBudget;
    private int givenCoinsEachRound;

    public GameConstrains() {
        this(2, 2, 2, 10, 10, 30,
                5, 4, 10, 2);
    }
    public GameConstrains(int playersNumber, int buildingRoundsNumber, int actualRoundsNumber, int arenaN, int arenaM, int storeSize, int maxPlayerArenaChampion, int maxPlayerBenchChampion, int initialBudget, int givenCoinsEachRound) {
        this.playersNumber = playersNumber;
        this.buildingRoundsNumber = buildingRoundsNumber;
        this.actualRoundsNumber = actualRoundsNumber;
        this.arenaN = arenaN;
        this.arenaM = arenaM;
        this.storeSize = storeSize;
        this.maxPlayerArenaChampion = maxPlayerArenaChampion;
        this.maxPlayerBenchChampion = maxPlayerBenchChampion;
        this.initialBudget = initialBudget;
        this.givenCoinsEachRound = givenCoinsEachRound;
    }

    public int getPlayersNumber() {
        return playersNumber;
    }
    public int getBuildingRoundsNumber() {
        return buildingRoundsNumber;
    }
    public int getActualRoundsNumber() {
        return actualRoundsNumber;
    }
    public int getArenaN() {
        return arenaN;
    }
    public int getArenaM() {
        return arenaM;
    }
    public int getStoreSize() {
        return storeSize;
    }
    public int getMaxPlayerArenaChampion() {
        return maxPlayerArenaChampion;
    }
    public int getMaxPlayerBenchChampion() {
        return maxPlayerBenchChampion;
    }
    public int getInitialBudget() {
        return initialBudget;
    }
    public int getGivenCoinsEachRound() {
        return givenCoinsEachRound;
    }

    public void setPlayersNumber(int playersNumber) {
        this.playersNumber = playersNumber;
    }
    public void setBuildingRoundsNumber(int buildingRoundsNumber) {
        this.buildingRoundsNumber = buildingRoundsNumber;
    }
    public void setActualRoundsNumber(int actualRoundsNumber) {
        this.actualRoundsNumber = actualRoundsNumber;
    }
    public void setArenaN(int arenaN) {
        this.arenaN = arenaN;
    }
    public void setArenaM(int arenaM) {
        this.arenaM = arenaM;
    }
    public void setStoreSize(int storeSize) {
        this.storeSize = storeSize;
    }
    public void setMaxPlayerArenaChampion(int maxPlayerArenaChampion) {
        this.maxPlayerArenaChampion = maxPlayerArenaChampion;
    }
    public void setMaxPlayerBenchChampion(int maxPlayerBenchChampion) {
        this.maxPlayerBenchChampion = maxPlayerBenchChampion;
    }
    public void setInitialBudget(int initialBudget) {
        this.initialBudget = initialBudget;
    }
    public void setGivenCoinsEachRound(int givenCoinsEachRound) {
        this.givenCoinsEachRound = givenCoinsEachRound;
    }

    public static int Read(String message) {
        try {
            System.out.print("Enter " + message + " : ");
            Scanner Reader = new Scanner(System.in);
            String x = Reader.nextLine();
            int X;
            try {
                X = Integer.parseInt(x);
            } catch (Exception exception) {
                throw new InvalidInput();
            }
            if(X <= 0) {
                throw new InvalidInput();
            }
            return X;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            System.out.println("Repeat");
            return Read(message);
        }
    }

    @Override
    public String toString() {
        return  "playersNumber : " + playersNumber +
                "\r\nbuilding Rounds Number : " + buildingRoundsNumber +
                "\r\nactual Rounds Number : " + actualRoundsNumber +
                "\r\narena N : " + arenaN +
                "\r\narena M : " + arenaM +
                "\r\nstore Size : " + storeSize +
                "\r\nmax Player Arena Champion : " + maxPlayerArenaChampion +
                "\r\nmax Player Bench Champion : " + maxPlayerBenchChampion +
                "\r\ninitial Budget : " + initialBudget +
                "\r\ngiven Coins Each Round : " + givenCoinsEachRound;
    }
}
