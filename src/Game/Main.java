package Game;

import Exceptions.InvalidInput;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean notDone = true;
        while (notDone)
        {
            System.out.println("\n\nTacticalChaos");
            System.out.println("0 - New Game\n"
                            + "1 - Resume Game\n"
                            + "2 - Exit\n");

            int choice = Read("");
            switch (choice) {
                case 0: (new TacticalChaosTM(GameConstrainsReader.Read())).StartGame(); break;
                case 1: (new TacticalChaosTM()).ResumeGame();                           break;
                case 2: notDone = false;                                                break;
            }
        }
    }

    public static int Read(String message) {
        try {
            System.out.print("Enter " + message + " : ");
            Scanner Reader = new Scanner(System.in);
            String x = Reader.nextLine();
            int X;
            try {
                X = Integer.parseInt(x);
            } catch (Exception exception) {
                throw new InvalidInput();
            }
            if(X < 0 || X > 2) {
                throw new InvalidInput();
            }
            return X;
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            System.out.println("Repeat");
            return Read(message);
        }
    }

}
