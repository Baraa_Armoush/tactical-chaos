package Game;

import java.io.Serializable;
import java.util.Scanner;

public interface GameConstrainsReader{
    public static GameConstrains Read() {
        System.out.println("Enter Game Constrains : ");
        System.out.println("\t0 - Default constrains");
        System.out.println("\totherwise - Custom constrains");
        Scanner Reader = new Scanner(System.in);
        System.out.print("Enter : ");   String x = Reader.nextLine();

        GameConstrains constrains = new GameConstrains();

        if(!x.equals("0")) {
            constrains.setPlayersNumber(GameConstrains.Read("Players Number"));
            constrains.setBuildingRoundsNumber(GameConstrains.Read("Building Round Numbers"));
            constrains.setActualRoundsNumber(GameConstrains.Read("Actual Round Numbers"));
            constrains.setArenaN(GameConstrains.Read("Arena n"));
            constrains.setArenaM(GameConstrains.Read("Arena m"));
            constrains.setStoreSize(GameConstrains.Read("Store size"));
            constrains.setMaxPlayerArenaChampion(GameConstrains.Read("Max player arena champions"));
            constrains.setMaxPlayerBenchChampion(GameConstrains.Read("Max player bench champions"));
            constrains.setInitialBudget(GameConstrains.Read("Initial budget"));
            constrains.setGivenCoinsEachRound(GameConstrains.Read("Given coins each round"));
        } else {
            System.out.println("Constrains : ");
            System.out.println(constrains);
        }

        return constrains;
    }
}
