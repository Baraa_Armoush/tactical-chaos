package Move;

import Arena.Square;
import Arena.SquareType;
import Champion.Champion;
import Exceptions.ChampionMoveLimitExceeded;
import Exceptions.ChampionNotFound;
import Player.Player;

import java.io.Serializable;

public class WalkMove extends Move {
    private WalkDirection walkDirection;

    public WalkMove(Player player, Champion champion, WalkDirection walkDirection) {
        super(player, champion);
        this.walkDirection = walkDirection;
    }

    public WalkDirection getWalkDirection() {
        return walkDirection;
    }
    public void setWalkDirection(WalkDirection walkDirection) {
        this.walkDirection = walkDirection;
    }

    @Override
    public synchronized void PerformMove() throws Exception {
        Square position = champion.getPosition();
        if(position == null) {
            throw new ChampionNotFound();
        }
        if (champion.getNumberOfMove() >= 3) {
            throw new ChampionMoveLimitExceeded();
        }
        int x = position.getX(), y = position.getY();
        position.getCurrentChampions().remove(champion);

        int ArenaN = player.getGame().getArena().getN();
        int ArenaM = player.getGame().getArena().getM();
        for (int i = 0; i < champion.getMovementSpeed(); i++) {
            switch (walkDirection) {
                case Up :   if (player.getGame().getArena().isValidSquare(x - 1, y))    x--;    break;
                case Down:  if (player.getGame().getArena().isValidSquare(x + 1, y))    x++;    break;
                case Left:  if (player.getGame().getArena().isValidSquare(x, y - 1))    y--;    break;
                case Right: if (player.getGame().getArena().isValidSquare(x, y + 1))    y++;    break;
            }
        }
        player.getGame().getArena().getSquare(x, y).getCurrentChampions().add(champion);
        champion.setPosition(player.getGame().getArena().getSquare(x, y));
        champion.setNumberOfMove(champion.getNumberOfMove() + 1);
        champion.EquipItems();
    }

    @Override
    public String toString() {
        return "Player" + player.getID() + " Move " + walkDirection + " Champion " + champion.getName();
    }
}
