package Move;

import java.io.Serializable;

public enum WalkDirection {
    Up,
    Down,
    Left,
    Right
}
