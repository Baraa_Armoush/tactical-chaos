package Move;

import Champion.Champion;
import Player.Player;

import java.io.Serializable;

public abstract class Move {
    protected Player player;
    protected Champion champion;

    public Move(Player player, Champion champion) {
        this.player = player;
        this.champion = champion;
    }

    public Player getPlayer() {
        return player;
    }
    public Champion getChampion() {
        return champion;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
    public void setChampion(Champion champion) {
        this.champion = champion;
    }

    public abstract void PerformMove() throws Exception;
}
