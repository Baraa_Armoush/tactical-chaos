package Move;

import Arena.Square;
import Champion.Champion;
import Exceptions.ChampionNotFound;
import Exceptions.NoEnoughMoney;
import Exceptions.TeamSizeLimitExceeded;
import Player.Player;

import java.io.Serializable;

public class BuyMove extends Move {
    private Square position;    // if position == null then put the champion in the bench , otherwise put it in the arena;

    public BuyMove(Player player, Champion champion, Square position) {
        super(player, champion);
        this.position = position;
    }

    public Square getPosition() {
        return position;
    }
    public void setPosition(Square position) {
        this.position = position;
    }

    @Override
    public synchronized void PerformMove() throws Exception {
        for(Champion champion1 : player.getGame().getStore().getChampions().getList()) {
            if(champion == champion1) {
                if(player.getBudget() < champion.getAttributes().getCost()) {
                    throw new NoEnoughMoney();
                }
                if(position != null) {
                    if(player.getBenchChampions().getList().size() + 1 > player.getGame().getConstrains().getMaxPlayerArenaChampion()) {
                        throw new TeamSizeLimitExceeded();
                    }
                    player.getArenaChampions().add(champion);
                    position.getCurrentChampions().add(champion);
                } else {
                    if(player.getBenchChampions().getList().size() + 1 > player.getGame().getConstrains().getMaxPlayerBenchChampion()) {
                        throw new TeamSizeLimitExceeded();
                    }
                    player.getBenchChampions().add(champion);
                }
                champion.setPosition(position);
                champion.EquipItems();
                champion.setCurrentPlayer(player);
                player.setBudget(player.getBudget() - champion.getAttributes().getCost());
                player.getGame().getStore().getChampions().remove(champion);
                player.CheckPromote(champion.getName());
                return ;
            }
        }
        throw new ChampionNotFound();
    }

    @Override
    public String toString() {
        return "Player" + player.getID() + " Buy Champion " + champion.getName();
    }
}
