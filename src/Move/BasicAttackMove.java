package Move;

import Champion.Champion;
import Damage.BasicAttackDamage;
import Exceptions.ChampionMoveLimitExceeded;
import Exceptions.ChampionNotFound;
import Exceptions.EnemyIsNotInAttackRange;
import Exceptions.EnemyNotFound;
import Player.Player;

import java.io.Serializable;

public class BasicAttackMove extends Move {
    private Champion enemy;

    public BasicAttackMove(Player player, Champion champion, Champion enemy) {
        super(player, champion);
        this.enemy = enemy;
    }

    public Champion getEnemy() {
        return enemy;
    }
    public void setEnemy(Champion enemy) {
        this.enemy = enemy;
    }

    @Override
    public synchronized void PerformMove() throws Exception {
        for(Champion champion1 : player.getArenaChampions().getList()) {
            if(champion1 == champion) {
                if(champion.getPosition() == null) {
                    throw new ChampionNotFound();
                }
                if (champion.getNumberOfMove() >= 3) {
                    throw new ChampionMoveLimitExceeded();
                }
                if(enemy.getPosition() == null) {
                    throw new EnemyNotFound();
                }
                if(!champion.isInAttackRange(enemy)) {
                    throw new EnemyIsNotInAttackRange();
                }
                champion.setNumberOfMove(champion.getNumberOfMove() + 1);
                enemy.GetIntendedDamage(new BasicAttackDamage(champion.generateBasicAttackDamage()));
                return;
            }
        }
        throw new ChampionNotFound();
    }

    @Override
    public String toString() {
        return "Champion " + champion.getName() + " of the Player" + player.getID() + " Basic Attack Champion " + enemy.getName();
    }
}
