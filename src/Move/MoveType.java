package Move;

import java.io.Serializable;

public enum MoveType {
    Buy,
    BuyForFree,
    Sell,
    Walk,
    Swap,
    BasicAttack;

    @Override
    public String toString() {
        switch (this) {
            case Buy: return "Buy";
            case BuyForFree: return "Buy For Free";
            case Sell:  return "Sell";
            case BasicAttack: return "Basic Attack";
            case Walk: return "Walk";
            case Swap: return "Swap";
            default:    return null;
        }
    }
}
