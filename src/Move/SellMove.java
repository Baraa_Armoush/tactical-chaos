package Move;

import Champion.Champion;
import Champion.ChampionFactory;
import Exceptions.ChampionMoveLimitExceeded;
import Exceptions.ChampionNotFound;
import Player.Player;

import java.io.Serializable;

public class SellMove extends Move {

    public SellMove(Player player, Champion champion) {
        super(player, champion);
    }

    @Override
    public synchronized void PerformMove() throws Exception {
        for(Champion champion1 : player.getArenaChampions().getList()) {
            if(champion == champion1) {
                if (champion.getNumberOfMove() >= 3) {
                    throw new ChampionMoveLimitExceeded();
                }
                player.getGame().getArena().DistributeItems(champion.getItems());
                player.getArenaChampions().remove(champion);
                champion.setNumberOfMove(champion.getNumberOfMove() + 1);
                champion.getPosition().getCurrentChampions().remove(champion);
                player.setBudget(champion.getCurrentPlayer().getBudget() + champion.getAttributes().getCost() / 2);
                player.getGame().getStore().getChampions().add(ChampionFactory.Create(champion.getName()));
                return ;
            }
        }
        for(Champion champion1 : player.getBenchChampions().getList()) {
            if(champion == champion1) {
                player.getBenchChampions().remove(champion);
                player.setBudget(champion.getCurrentPlayer().getBudget() + champion.getAttributes().getCost() / 2);
                player.getGame().getStore().getChampions().add(ChampionFactory.Create(champion.getName()));
                return ;
            }
        }
        throw new ChampionNotFound();
    }

    @Override
    public String toString() {
        return "Player" + player.getID() + " Sell Champion " + champion.getName();
    }
}
