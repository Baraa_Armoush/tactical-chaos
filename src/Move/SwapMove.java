package Move;

import Champion.Champion;
import Exceptions.ChampionMoveLimitExceeded;
import Exceptions.ChampionNotFound;
import Player.Player;

import java.io.Serializable;

public class SwapMove extends Move {
    private Champion benchChampion;

    public SwapMove(Player player, Champion champion, Champion benchChampion) {
        super(player, champion);
        this.benchChampion = benchChampion;
    }

    public Champion getBenchChampion() {
        return benchChampion;
    }
    public void setBenchChampion(Champion benchChampion) {
        this.benchChampion = benchChampion;
    }

    @Override
    public synchronized void PerformMove() throws Exception {
        for(Champion champion1 : player.getArenaChampions().getList()) {
            if(champion1 == champion) {
                for(Champion champion2 : player.getBenchChampions().getList()) {
                    if(champion2 == benchChampion) {
                        if (champion.getNumberOfMove() >= 3) {
                            throw new ChampionMoveLimitExceeded();
                        }
                        champion1.setNumberOfMove(champion1.getNumberOfMove() + 1);
                        champion2.setNumberOfMove(champion2.getNumberOfMove() + 1);
                        player.getArenaChampions().remove(champion1);   player.getArenaChampions().add(champion2);
                        player.getBenchChampions().remove(champion2);   player.getBenchChampions().add(champion1);
                        champion2.setPosition(champion1.getPosition());
                        champion2.EquipItems();
                        champion1.getPosition().getCurrentChampions().remove(champion1);
                        champion1.getPosition().getCurrentChampions().add(champion2);
                        champion1.setPosition(null);
                        return ;
                    }
                }
            }
        }
        throw new ChampionNotFound();
    }

    @Override
    public String toString() {
        return "Player" + player.getID() + " Swap Champions " + champion.getName() + ", " + benchChampion.getName();
    }
}
