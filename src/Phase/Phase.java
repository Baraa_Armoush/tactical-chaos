package Phase;

import Game.Game;

import java.io.Serializable;

public abstract class Phase {
    private String phaseType;

    public Phase(String phaseType) {
        this.phaseType = phaseType;
    }

    public abstract void Run(Game game);
}
