package Phase;

import Game.Game;
import Player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Executing extends Phase {
    public Executing() {
        super("executing");
    }

    @Override
    public void Run(Game game) {
        List<Player> players = new ArrayList<Player>(game.getCurrentPlayers());
        Collections.shuffle(players);

        Thread[] threads = new Thread[ players.size() ];
        for (int i = 0; i < players.size(); i++) {
            threads[i] = new Thread(players.get(i));
            threads[i].start();
        }
        for (int i = 0; i < players.size(); i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "Executing phase";
    }
}
