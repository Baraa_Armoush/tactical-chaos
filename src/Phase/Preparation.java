package Phase;

import Filter.TemporalStoreFilter;
import Game.Game;
import Move.MoveType;
import Player.Player;
import Store.Store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Preparation extends Phase {
    public Preparation() {
        super("preparation");
    }

    @Override
    public void Run(Game game) {
        List<MoveType> availableArenaChampionsMoves = new ArrayList<MoveType>(List.of());
        List<MoveType> availableBenchChampionsMoves = new ArrayList<MoveType>(List.of());
        List<MoveType> availableStoreChampionsMoves = new ArrayList<MoveType>(List.of(MoveType.BuyForFree));
        for(Player player : game.getCurrentPlayers()) {
            Store temporalStore = game.getStore().AcceptFilter(new TemporalStoreFilter());
            player.Display("Player " + player.getID() + " : ");
            while(player.PropagateMove(availableArenaChampionsMoves, availableBenchChampionsMoves, availableStoreChampionsMoves, temporalStore)) {
                player.Display("------------");
            }
            player.Display("----------------------------------");
        }
    }

    @Override
    public String toString() {
        return "Preparation phase";
    }
}
